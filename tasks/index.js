const axios = require('axios');

module.exports = ({ token, URL_BASE }) => {

	const setStatus = (taskTarget, status) => {
		// Format the headers for the request
		const headers = {
			Authorization: `Bearer ${token}`,
		};

		// Create a new workflow object
		return axios({
			method: 'GET',
			url: `${URL_BASE}/tasks/${taskTarget}?fetchOptions=true`,
			headers,
		})
			.then((res) => {
				const { task } = res.data.data;
				if (task.status !== 'error' || !task.options.includes(status)) {
					return false;
				}
				if (task.optionsEnforcement && Array.isArray(task.optionsEnforcement) && task.optionsEnforcement[status].length > 0) {
					return false;
				}

				// Update status to inactive
				const optionsInit = {
					method: 'PUT',
					url: `${URL_BASE}/tasks/${taskTarget}/status`,
					headers,
					json: true,
					data: {
						status: status
					}
				};
				return axios(optionsInit);
			});
	};

	return {
		reinitializeTask,
	};
};
