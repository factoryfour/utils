const workflows = require('./workflows');

/*
 * Retrieve a valid FactoryFour token from the headers in the context
 */
const getToken = (ctx) => {
	const token = ctx.headers['x-factoryfour-token'];
	if (!token) {
		throw new Error('Unable to find token in context');
	}
	return token;
};

module.exports = (context) => {
	const token = getToken(context);

	return {
		workflows: workflows(token),

	}
}
